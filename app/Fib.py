#!/usr/bin/python

from math import *
import time

def recur_fibo(n):
   if n <= 1:
       return n
   else:
       return(recur_fibo(n-1) + recur_fibo(n-2))

def fast_fibo(n):
    if n<=1:
        return n
    elif n==2:
        return 1
    else:
        f = (((1+sqrt(5))/2)**n)/sqrt(5)
        return round(f)

start_time = time.time()
x = recur_fibo(35)
print("recur_fibo :",x,"\ntemps",time.time()-start_time,"s\n")
start_time = time.time()
x = fast_fibo(35)
print("fast_fibo :",x,"\ntemps",time.time()-start_time,"s\n")